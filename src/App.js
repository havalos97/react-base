import React, { Component, Fragment } from "react";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<Fragment>
				<h1>Welcome to React.JS</h1>
			</Fragment>
		);
	}
}

export default App;
